package model.obstacle;

/**
 * Specify the shape of the given {@link Obstacle}.
 */

public enum ObstacleType {

    /**
     * If the {@link Obstacle} is a Circle.
     */
    CIRCLE,

    /**
     * if the {@link Obstacle} is a Rectangle.
     */
    RECTANGLE;

}
