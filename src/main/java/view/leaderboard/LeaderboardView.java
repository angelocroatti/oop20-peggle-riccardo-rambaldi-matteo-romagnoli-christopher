package view.leaderboard;

/**
 * LeaderboardView allows to show the leaderboard menu.
 *
 */
public interface LeaderboardView {

    /**
     * Shows the leaderboard menu.
     */
    void show();
}
