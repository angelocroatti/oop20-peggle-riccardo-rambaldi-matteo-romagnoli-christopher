package view.menu;

/**
 * ChoiceMenuView allows to show the choice menu.
 */
public interface ChoiceMenuView {

    /**
     * Shows the choice menu.
     */
    void show();
}
